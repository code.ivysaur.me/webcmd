package webcmd

import (
	"net/http"
)

func (this *App) Action_AbandonTask(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		fail(w, r, err.Error())
		return
	}

	taskRef := r.Form.Get("task_ref")

	this.tasksMtx.RLock()
	task, ok := this.tasks[taskRef]
	this.tasksMtx.RUnlock()

	if !ok {
		http.Error(w, "Invalid task specified", 404)
		return
	}

	task.cancel()
	http.Redirect(w, r, "/task/"+taskRef, 302)
}

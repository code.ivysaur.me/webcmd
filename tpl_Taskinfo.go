package webcmd

import (
	"fmt"
	"net/http"
	"time"
)

func (this *App) Serve_TaskInfo(w http.ResponseWriter, task_ref string) {

	this.tasksMtx.RLock()
	taskinfo, ok := this.tasks[task_ref]
	this.tasksMtx.RUnlock()

	if !ok {
		http.Error(w, "Unknown task.", 404)
		return
	}

	w.Header().Set("Content-Type", "text/html;charset=UTF-8")
	w.WriteHeader(200)

	this.ServePartial_Header(w, "/taskinfo/...")

	fmt.Fprintf(w, `
		<ul>
			<li>Start time: %s</li>
	`,
		hesc(time.Unix(taskinfo.started, 0).Format(time.RFC822Z)),
	)

	if taskinfo.Finished() {
		fmt.Fprintf(w, `
			<li>Completed at: %s</li>
			<li>Process exit code: %d</li>
		`,
			hesc(time.Unix(taskinfo.stopped, 0).Format(time.RFC822Z)),
			taskinfo.exitCode,
		)
	} else {
		fmt.Fprint(w, `<li>Currently running</li>`)
	}

	fmt.Fprint(w, `
		</ul>
		
		<table id="task-output">
			<thead>
				<tr>
					<th>Message</th>
				</tr>
			</thead>
			<tbody>
	`)

	for _, line := range taskinfo.output {
		messageClass := "stdout"
		if line.isError {
			messageClass = "stderr"
		}
		fmt.Fprintf(w,
			`<tr class="%s">
				<td>%s</td>
			</tr>
		`, messageClass, hesc(line.text),
		)
	}

	fmt.Fprintf(w, `
			</tbody>
		</table>
	`)

	if !taskinfo.Finished() {
		fmt.Fprintf(w, `
		<form method="POST" action="/x-abandon-task">
			<input type="hidden" name="task_ref" value="%s">
			<input type="submit" value="Cancel &raquo;">
		</form>
		`,
			hesc(task_ref),
		)
	}

	this.ServePartial_Footer(w)
}

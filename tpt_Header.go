package webcmd

import (
	"fmt"
	"net/http"
)

func (this *App) ServePartial_Header(w http.ResponseWriter, slug string) {

	fmt.Fprint(w, `<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>`+hesc(this.cfg.AppTitle)+`</title>
		<link rel="stylesheet" type="text/css" href="/style.css">
		<meta viewport="width=device-width, initial-scale=1">
	</head>
	<body>
		<h2>`+hesc(this.cfg.AppTitle)+`</h2>
	`)

	if slug == "/" {
		fmt.Fprint(w, "New task")
	} else {
		fmt.Fprint(w, `<a href="/">New task</a>`)
	}

	fmt.Fprint(w, ` | `)

	if slug == "/tasks" {
		fmt.Fprint(w, "Current tasks")
	} else {
		fmt.Fprint(w, `<a href="/tasks">Current tasks</a>`)
	}
}

func (this *App) ServePartial_Footer(w http.ResponseWriter) {
	fmt.Fprint(w, `
	</body>
</html>
	`)
}

package webcmd

import (
	"fmt"
	"log"
	"net/http"
)

func (this *App) Serve_Homepage(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/html;charset=UTF-8")
	w.WriteHeader(200)

	this.ServePartial_Header(w, "/")

	for i, t := range this.cfg.Commands {
		fmt.Fprint(w,
			`<h3>`+hesc(t.Title)+`</h3>
			<form method="POST" action="/x-new-task">
				<input type="hidden" name="task_id" value="`+fmt.Sprintf("%d", i)+`">
		`)
		for i, param := range t.Execution {
			switch param.ParamType {
			case PARAMTYPE_CONST:
				// not configurable parameter
			case PARAMTYPE_STRING:
				fmt.Fprintf(w, `<input type="text" name="param[%d]" placeholder="%s" value="%s"><br>`,
					i, hesc(param.Description), hesc(param.Value))
			case PARAMTYPE_OPTIONAL:
				fmt.Fprintf(w, `<input type="hidden" name="param[%d]" value="off"><label><input type="checkbox" name="param[%d]" value="on">%s</label><br>`,
					i, i, hesc(param.Description))
			default:
				log.Fatalf("Unknown PARAMTYPE(%d)", param.ParamType)
			}
		}
		fmt.Fprint(w, `
				<input type="submit" value="Run &raquo;">
			</form>
		`)
	}

	this.ServePartial_Footer(w)
}

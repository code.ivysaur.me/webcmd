package webcmd

import (
	"fmt"
	"net/http"
	"strconv"
)

func (this *App) Action_NewTask(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		fail(w, r, fmt.Sprintf("Bad form data: %s", err.Error()))
		return
	}

	taskIdStr := r.Form.Get("task_id")
	if len(taskIdStr) == 0 {
		fail(w, r, "Missing task ID in request")
		return
	}

	taskId, err := strconv.Atoi(taskIdStr)
	if err != nil {
		fail(w, r, err.Error())
		return
	}

	if taskId < 0 || taskId >= len(this.cfg.Commands) {
		fail(w, r, fmt.Sprintf("Invalid task ID %d", taskId))
		return
	}

	taskInfo := this.cfg.Commands[taskId]

	params := make([]string, 0, len(taskInfo.Execution))
	for i, prop := range taskInfo.Execution {
		switch prop.ParamType {
		case PARAMTYPE_CONST:
			params = append(params, prop.Value)

		case PARAMTYPE_STRING:
			val := r.Form.Get(fmt.Sprintf("param[%d]", i))
			params = append(params, val)

		case PARAMTYPE_OPTIONAL:
			val := r.Form.Get(fmt.Sprintf("param[%d]", i))
			if val == "on" {
				params = append(params, prop.Value)
			} else if val == "off" {
				// nothing
			} else {
				fail(w, r, "Unexpected value for parameter")
				return
			}

		}
	}

	// Create new command from supplied values
	taskRef, err := this.LaunchTask(taskInfo.WorkingDir, params)
	if err != nil {
		fail(w, r, err.Error())
		return
	}

	http.Redirect(w, r, "/task/"+taskRef, 302)
}

package webcmd

import (
	"fmt"
	"net/http"
	"sort"
	"time"
)

type TaskListItem struct {
	ref   string
	start int64
}

type TaskList []TaskListItem

func (tl TaskList) Len() int {
	return len(tl)
}

func (tl TaskList) Swap(i, j int) {
	tl[i], tl[j] = tl[j], tl[i]
}

func (tl TaskList) Less(i, j int) bool {
	return tl[i].start < tl[j].start
}

func (this *App) Serve_Tasks(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/html;charset=UTF-8")

	w.WriteHeader(200)
	this.ServePartial_Header(w, "/tasks")

	fmt.Fprint(w, `
		<table>
			<thead>
				<tr>
					<th>Task</th>
					<th>Started</th>
					<th>State</th>
				</tr>
			</thead>
			<tbody>
	`)

	this.tasksMtx.RLock()
	defer this.tasksMtx.RUnlock()

	tl := make(TaskList, 0, len(this.tasks))
	for ref, t := range this.tasks {
		tl = append(tl, TaskListItem{ref, t.started})
	}
	sort.Sort(tl)

	for _, tlRef := range tl {
		ref := tlRef.ref
		t := this.tasks[ref]

		startTime := time.Unix(t.started, 0)

		fmt.Fprintf(w,
			`<tr>
				<td><a href="/task/%s">%s</td>
				<td><span title="%s">%s</span></td>
				<td>
		`,
			hesc(ref), hesc(ref),
			hesc(startTime.Format(time.RFC3339)),
			hesc(startTime.Format(time.RFC822)),
		)

		if t.Finished() {
			fmt.Fprint(w, `<span class="task-state-finished">Finished</span>`)
		} else {
			fmt.Fprint(w, `<span class="task-state-running">Running</span>`)
		}

		fmt.Fprint(w, `
				</td>
			</tr>
		`)
	}

	fmt.Fprint(w, `
			</tbody>
		</table>
		
		<form method="POST" action="/x-clear-completed-tasks">
			<input type="submit" value="Clear completed tasks &raquo;">
		</form>
	`)
	this.ServePartial_Footer(w)
}

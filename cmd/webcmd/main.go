package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"code.ivysaur.me/webcmd"
)

func main() {
	confPath := flag.String("config", "webcmd.conf", "Path to configuration file")
	versionFlag := flag.Bool("version", false, "Display version number and exit")
	flag.Parse()

	if *versionFlag {
		fmt.Println("webcmd/" + webcmd.APP_VERSION)
		os.Exit(0)
	}

	app, err := webcmd.NewApp(*confPath)
	if err != nil {
		log.Fatalf(err.Error())
	}

	app.Run()
}

package webcmd

import (
	"encoding/json"
	"errors"
)

type ParamType int

const (
	PARAMTYPE_CONST    ParamType = 0
	PARAMTYPE_STRING   ParamType = 1
	PARAMTYPE_OPTIONAL ParamType = 2
	// bool 1/0
	// list
	// k/v list
	// file upload (temporary path passed to binary)
	// nested parse subgroup (e.g. ffmpeg filters)
	// one optional to control a whole subgroup (e.g. --timeout 4)
	// String validations (regexp, min-length, ...)
)

type InputParam struct {
	Description string `json:",omitempty"` // only used for editable parameters
	ParamType   ParamType
	Value       string
}

func (ip *InputParam) UnmarshalJSON(b []byte) error {
	switch b[0] {
	case '"':
		ip.Description = ""
		ip.ParamType = PARAMTYPE_CONST
		tmp := ""
		err := json.Unmarshal(b, &tmp)
		if err != nil {
			return err
		}
		ip.Value = tmp

	case '{':
		read := struct {
			Description string `json:",omitempty"`
			ParamType   ParamType
			Value       string
		}{}
		err := json.Unmarshal(b, &read)
		if err != nil {
			return err
		}
		*ip = read
		return nil

	default:
		return errors.New("Malformed InputParam")
	}

	return nil
}

type CommandConfig struct {
	Title      string
	WorkingDir string       // default empty-string: getcwd()
	Execution  []InputParam // Can be unmarshalled using plain strings
}

type AppConfig struct {
	ListenAddress   string
	AppTitle        string
	MaxHistoryLines int // default zero: unlimited history for each command
	Commands        []CommandConfig
}

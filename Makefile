.PHONY: all clean

VERSION := 1.0.2

OBJS := webcmd-$(VERSION)-linux64.tar.xz \
	webcmd-$(VERSION)-linux32.tar.xz \
	webcmd-$(VERSION)-win64.zip \
	webcmd-$(VERSION)-win32.zip

XZ_OPTS=-9

all: $(OBJS)

define compilelinux
	/bin/bash -c 'GOARCH=$(1) GOOS=linux go build -a -o ./webcmd -ldflags "-s -w -X code.ivysaur.me/webcmd.APP_VERSION=$(VERSION)" -gcflags "-trimpath=$(GOPATH)" ./cmd/webcmd'
	tar caf webcmd-$(2)-$(VERSION).tar.xz webcmd -C cmd/webcmd webcmd.conf-sample --owner=0 --group=0
	rm webcmd
endef

define compilewin
	/bin/bash -c 'GOARCH=$(1) GOOS=windows go build -a -o ./webcmd -ldflags "-s -w -X code.ivysaur.me/webcmd.APP_VERSION=$(VERSION)" -gcflags "-trimpath=$(GOPATH)" ./cmd/webcmd'
	zip -j webcmd-$(2)-$(VERSION).zip webcmd cmd/webcmd/webcmd.conf-sample
	rm webcmd
endef

webcmd-$(VERSION)-linux64.tar.xz:
	$(call compilelinux,amd64,linux64)

webcmd-$(VERSION)-linux32.tar.xz:
	$(call compilelinux,386,linux32)
	
webcmd-$(VERSION)-win64.zip:
	$(call compilewin,amd64,win64)
	
webcmd-$(VERSION)-win32.zip:
	$(call compilewin,386,win32)
	
clean:
	rm $(OBJS)

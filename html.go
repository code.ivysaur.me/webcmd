package webcmd

import (
	"html"
	"log"
	"net/http"
)

func hesc(in string) string {
	return html.EscapeString(in)
}

func fail(w http.ResponseWriter, r *http.Request, s string) {
	log.Printf("[%s] %s", r.RemoteAddr, s)
	http.Error(w, "Malformed request", 400)
}

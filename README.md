# webcmd

A web-based interface for arbitrary command-line tools.

Configure the possible options for your command (e.g. optional flags, custom arguments) and then remotely invoke the tool via a web interface. The web interface displays stdout/stderr of recent and in-progress tasks, and can cancel a long-running command at any time.

## Usage

```
Usage of ./webcmd:
  -config string
    	Path to configuration file (default "webcmd.conf")
  -version
    	Display version number and exit
```

## Configuration

See the included `webcmd.conf-sample` file for an example configuration implementing a Looking Glass server over the top of `/bin/ping`.

## Changelog

2017-03-27 1.0.1
- Enhancement: Simplify configuration file format, allow parsing const strings directly
- Enhancement: Improve time format display
- Fix an issue with pasting into form fields on Firefox Mobile
- Fix an issue with not consistently interpreting `MaxHistoryLines:0` as infinite history
- Fix a cosmetic issue with mobile viewports
- Fix a cosmetic issue with task ordering
- [⬇️ webcmd-win64-1.0.1.zip](https://git.ivysaur.me/attachments/627d5246-0f51-4546-99b1-61d9b9b0cc0b) *(1.46 MiB)*
- [⬇️ webcmd-win32-1.0.1.zip](https://git.ivysaur.me/attachments/92de57b3-6f5c-4ba4-ac43-9f85c89041dd) *(1.38 MiB)*
- [⬇️ webcmd-src-1.0.1.zip](https://git.ivysaur.me/attachments/120a0a7c-52ab-4bb9-94fe-40d269b0669d) *(11.51 KiB)*
- [⬇️ webcmd-linux64-1.0.1.tar.xz](https://git.ivysaur.me/attachments/41d001ed-11b7-499c-ba18-1a22fe0ed140) *(1.14 MiB)*
- [⬇️ webcmd-linux32-1.0.1.tar.xz](https://git.ivysaur.me/attachments/0cb47392-36d1-4f48-9ee6-271f06dba952) *(1.06 MiB)*

2017-03-25 1.0.0
- Initial public release
- [⬇️ webcmd-win64-1.0.0.zip](https://git.ivysaur.me/attachments/6442cde4-0dfe-432d-9cb3-42272c8e6551) *(1.46 MiB)*
- [⬇️ webcmd-win32-1.0.0.zip](https://git.ivysaur.me/attachments/261489d4-8393-450c-9f72-1bca009e0942) *(1.37 MiB)*
- [⬇️ webcmd-src-1.0.0.zip](https://git.ivysaur.me/attachments/cbac2a73-2af0-4c60-ab0a-9970250c9a6e) *(10.50 KiB)*
- [⬇️ webcmd-linux64-1.0.0.tar.xz](https://git.ivysaur.me/attachments/a713a0f5-e3de-442c-a090-26ae49b03ac9) *(1.14 MiB)*
- [⬇️ webcmd-linux32-1.0.0.tar.xz](https://git.ivysaur.me/attachments/86d5e65e-c3da-4741-a0c0-f4ec2e123689) *(1.06 MiB)*
package webcmd

import (
	"fmt"
	"net/http"
)

func (this *App) Serve_StyleCSS(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/css")
	w.WriteHeader(200)
	fmt.Fprint(w, `
/* Global styles */ 
html,body {
	font-family: sans-serif;
}
a {
	color:green;
}
th {
	text-align:left;
}
table {
	width:100%;
	border-collapse: collapse;
	margin-top:1em;
}
td {
	padding:2px;
}
tr:hover td {
	background:lightyellow;
}

/* Specific elements */
.task-state-finished {
	font-weight:bold;
}
.task-state-running {
	color:darkgreen;
}
#task-output tbody {
	font-family: monospace;
}
#task-output .stdout {
}
#task-output .stderr {
	color:red;
}
`)
}

package webcmd

import (
	"net/http"
)

func (this *App) Action_ClearCompleted(w http.ResponseWriter, r *http.Request) {

	this.tasksMtx.Lock()
	defer this.tasksMtx.Unlock()

	for k, v := range this.tasks {
		if v.Finished() {
			delete(this.tasks, k)
		}
	}

	http.Redirect(w, r, "/tasks", 302)
}
